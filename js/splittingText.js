
function initSplittingText (options) {
  //  settings
  const splittingClass = options.className
  const domElement = options.element
  const staggering = options.staggering
  const staggeringAll = options.staggeringAll
  const delay = options.delay

  //  get all elements
  const elements = document.querySelectorAll('[data-splitting-text]')

  //  create new elements
  elements.forEach(function (element) {
    const textInside = element.innerHTML
    const textLength = textInside.length

    const newElements = []

    element.innerHTML = '' // delete inital text

    for (let i = 0; i < textLength; i++) {
      const newElement = document.createElement(domElement) // create span ELement

      newElement.classList.add(splittingClass)

      //  for staggering Animation
      if (staggering) {
        newElement.style.animationDelay = delay * i + 's'
      }

      if (textInside[i] === ' ') {
        newElement.innerHTML = '&nbsp;'
      } else {
        newElement.innerHTML = textInside[i]
      }

      newElements.push(newElement)
    };

    newElements.forEach(function (arrElement) {
      element.appendChild(arrElement)
    })
  })

  //  if staggeringAll is set to true add constant delay to all (staggering has to be unselected)
  if (staggeringAll) {
    const splittedElements = document.querySelectorAll('.' + splittingClass)
    const splittedElementsLength = splittedElements.length

    for (let i = 0; i < splittedElementsLength; i++) {
      splittedElements[i].style.animationDelay = delay * i + 's'
    };
  }

  if (staggeringAll && staggering) {
    console.log('splittingText.js: you can not use staggering AND staggeringAll')
  }
}
